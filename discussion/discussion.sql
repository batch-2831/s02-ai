

-- Command to access our MariaDB in the cmd
mysql-u root


-- List down the databases in the RDBMS
SHOW DATABASES

-- show databases; small caps also work but this is not the best practice.

 -- create a database
CREATE DATABASE music_db;

-- drop a database
DROP DATABASE music_db;


-- Select a database to manipulate
USE music_db;

-- Command to access MariaDB in cmd.
mysql -u root

-- Lists down the databases inside the RDBMS.
SHOW DATABASES;

-- show databases; small caps will also work but this is not the best practice.

-- Create a database.
CREATE DATABASE music_db;

-- Drop database
DROP DATABASE music_db;

-- Select a database to manipulate.
USE music_db;

-- [SECTION] Creating Tables
-- Table columns have the following format: [column_name] [data_type] [other_options]

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number INT NOT NULL,
    email VARCHAR(50),
    address VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE artists (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE albums (
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    data_released DATE NOT NULL,
    artist_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_albums_artist_id
        FOREIGN KEY (artist_id)
            REFERENCES artists(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
);

CREATE TABLE songs (
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    length TIME NOT NULL,
    genre VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_songs_album_id
        FOREIGN KEY(album_id) REFERENCES albums(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT

)

CREATE TABLE playlist(
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlist_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

CREATE TABLE playlist_songs(
    id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_songs_playlist_id
        FOREIGN KEY (playlist_id) REFERENCES playlist(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_playlist_songs_songs_id
        FOREIGN KEY (songs_id) REFERENCES songs(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


CREATE TABLE reviews (
    id INT NOT NULL AUTO_INCREMENT,
    review VARCHAR(50) NOT NULL,
    PRIMARY KEY(id)
);

-- display all tables
SHOW TABLES;

-- drops a specific table
DROP TABLE tablename;

-- shows detail of the table in table format
DESCRIBE tablename;

-- to drop a column from a table
ALTER TABLE reviews DROP COLUMN review;


-- to add new column in a table
ALTER TABLE reviews ADD feedback VARCHAR(300) NOT NULL;

-- to add new column in a specific placement
ALTER TABLE reviews ADD feedback VARCHAR(300) NOT NULL
AFTER id;


ALTER TABLE reviews CHANGE COLUMN comments review VARCHAR(300) NOT NULL; 

